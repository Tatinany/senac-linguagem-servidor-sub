<?php
class Usuario {
    private $id;
    private $nome;
    private $senha;
    private $email;
    private $cpf;
    private $objDb;

    public function __construct(){
        $this->objDb = new mysqli('localhost','root','','senac-linguagem-servidor-sub',8443);
    }
    
    public function setId(int $id){
        $this->id = $id;
    }

    public function setNome(string $nome){
        $this->nome = $nome;
    }

    public function setSenha(string $senha){
        $this->senha = $senha;
    }

    public function setEmail(string $email){
        $this->email = $email;
    }

    public function setCpf(int $cpf){
        $this->cpf = $cpf;
    }

    public function getId() : int{
        return $this->id;
    }

    public function getNome() : string{
        return $this->nome;
    }

    public function getEmail() : string{
        return $this->email;
    }

    public function getSenha() : string{
        return $this->senha;
    }

    public function getCpf() : int{
        return $this->cpf;
    }

    public function saveUsuario(){
    
        $objStmt = $this->objDb->prepare('REPLACE INTO cliente(senha, nome, email, cpf) VALUES (?,?,?,?)');
        $objStmt->bind_param('sssi', $this->senha, $this->nome, $this->email, $this->cpf);

        return $objStmt-> execute();
    }

    public function deleteUsuario(){
        $objStmt = $this->objDb->prepare('DELETE FROM cliente WHERE id = ?');
        $objStmt->bind_param('i', $this->id);

        return $objStmt-> execute();
    } 

    
    public function findUsuarios(){
        $objStmt = $this->objDb->prepare('SELECT id, nome, email, cpf FROM cliente WHERE senha=? AND email=?');
        $objStmt->bind_param('ss', $this->senha, $this->email);
        $objStmt->execute();
        $objStmt->bind_result($id_u, $nome_u, $email_u, $cpf_u);

        while ($objStmt->fetch()) {
            session_start();
            $_SESSION['userid'] = $id_u;
            $_SESSION['usernome'] = $nome_u;
            $_SESSION['useremail'] = $email_u;
            $_SESSION['usercpf'] = $cpf_u;
        } 
        $findUser=$objStmt->num_rows > 0;
        $objStmt->close();
        return $findUser;
    }

    public function __destruct(){
        unset($this->objDb);
    }
}