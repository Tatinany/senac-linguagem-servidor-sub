<?php

    require_once('../usuario.class.php');
    $objUsuario = new Usuario;

    $nome = $_POST['nome_cad'] ;
    $senha = $_POST['senha_cad'];
    $email = $_POST['email_cad'];
    $cpf = $_POST['cpf_cad'];

    $objUsuario->setNome($nome);
    $objUsuario->setEmail($email);
    $objUsuario->setSenha($senha);
    $objUsuario->setCpf($cpf);

    if($objUsuario->saveUsuario()){
        header('Location: ../index.php');
    }else{
        echo 'Ops, estamos passando por problemas, por favor tente mais tarde.';
        echo '<br/><a href="login-cadastro.html">Voltar</a><br><br>';
    }

?>

