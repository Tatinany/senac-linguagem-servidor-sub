<?php

    require_once('../usuario.class.php');
    $objUsuario = new Usuario;
    
    $senha = $_POST['senha_login'];
    $email = $_POST['email_login'];

    $objUsuario->setEmail($email);
    $objUsuario->setSenha($senha);

    
    if($objUsuario->findUsuarios()){
        session_start();
        $_SESSION['session_user_id'] = $_SESSION['userid'];
        header('Location: ../index.php');
    }else{
        echo 'Ops, usuario nao encontrado, por favor tente mais tarde ou se cadastre.';
        echo '<br/><a href="login-cadastro.html">Voltar</a><br><br>';
    }

?>