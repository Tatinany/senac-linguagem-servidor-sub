<?php
class Usuario {
    private $id;
    private $idCliente;
    private $matricula;
    private $modelo;
    private $ano;
    private $cor;
    private $objDb;

    public function __construct(){
        $this->objDb = new mysqli('localhost','root','','senac-linguagem-servidor-sub',8443);
    }
    
    public function setId(int $id){
        $this->id = $id;
    }

    public function setIdCliente(int $idCliente){
        $this->idCliente = $idCliente;
    }

    public function setMatricula(string $matricula){
        $this->matricula = $matricula;
    }

    public function setModelo(string $modelo){
        $this->modelo = $modelo;
    }

    public function setAno(string $ano){
        $this->ano = $ano;
    }

    public function setCor(string $cor){
        $this->cor = $cor;
    }

    public function getId() : int{
        return $this->id;
    }

    public function getIdCliente() : int{
        return $this->idCliente;
    }

    public function getMatricula() : string{
        return $this->matricula;
    }

    public function getModelo() : string{
        return $this->modelo;
    }

    public function getAno() : string{
        return $this->ano;
    }

    public function getCor() : string{
        return $this->cor;
    }

    public function saveAeronave(){
    
        $objStmt = $this->objDb->prepare('REPLACE INTO aeronave(id_cliente, matricula, modelo, ano, cor) VALUES (?,?,?,?,?)');
        $objStmt->bind_param('issss', $this->idCliente, $this->matricula, $this->modelo, $this->ano, $this->cor);

        return $objStmt-> execute();
    }

    public function deleteAeronave(){
        $objStmt = $this->objDb->prepare('DELETE FROM aeronave WHERE id = ?');
        $objStmt->bind_param('i', $this->id);

        return $objStmt-> execute();
    } 

    
    public function findAeronaves(){
        $objStmt = $this->objDb->prepare('SELECT * FROM aeronave WHERE id_cliente=?');
        $objStmt->bind_param('i', $this->idCliente);
        $objStmt->execute();
        $objStmt->bind_result($id_a, $idCliente_a, $matricula_a, $modelo_a, $ano_a, $cor_a);

        while ($objStmt->fetch()) {
            $_SESSION['userid'] = $id_u;
            $_SESSION['usernome'] = $nome_u;
            $_SESSION['useremail'] = $email_u;
            $_SESSION['usercpf'] = $cpf_u;
        } 
        $findUser=$objStmt->num_rows > 0;
        $objStmt->close();
        return $findUser;
    }

    public function __destruct(){
        unset($this->objDb);
    }
}